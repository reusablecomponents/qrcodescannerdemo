//
//  QRCodeVC.swift
//  QRCodeScannerDemo
//
//  Created by signity on 26/06/17.
//  Copyright © 2017 signity. All rights reserved.
//

import UIKit
import AVFoundation
import SystemConfiguration

class QRCodeVC: UIViewController{
    
    @IBOutlet var result: UILabel!
    @IBOutlet var displayView: UIView!
    @IBOutlet var resultLabel: UILabel!
    @IBOutlet var btnScan: UIButton!
    
    
    
    // MARK:- Refrences.
    var captureSession    :AVCaptureSession?
    var videoPreviewLayer :AVCaptureVideoPreviewLayer?
    var qrCodeFrameView   :UIView?
    var qrResult          : String?
    
    var activityIndicator = UIActivityIndicatorView()
    var strLabel = UILabel()
    let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    
    let supportedCodeTypes = [AVMetadataObject.ObjectType.upce,
                              AVMetadataObject.ObjectType.code39,
                              AVMetadataObject.ObjectType.code39Mod43,
                              AVMetadataObject.ObjectType.code93,
                              AVMetadataObject.ObjectType.code128,
                              AVMetadataObject.ObjectType.ean8,
                              AVMetadataObject.ObjectType.ean13,
                              AVMetadataObject.ObjectType.aztec,
                              AVMetadataObject.ObjectType.pdf417,
                              AVMetadataObject.ObjectType.qr]
    
  
    // MARK:- Life Cycle.
    override func viewDidLoad() {
        super.viewDidLoad()
        displayView.isHidden = false
        result.isHidden = true
        resultLabel.isHidden = true
    }

    @IBAction func scanMorePressed(_ sender: Any) {
        if EventManager.checkForInternetConnection() {
                qrResult = nil
                self.displayView.isHidden = true
                SetupCaptureSession()
            }else{
            EventManager.showAlert(alertMessage: ConstantModel.api.NoInternet,
                                   btn1Tit: "Ok",
                                   btn2Tit: nil,
                                   sender: self,
                                   action: nil)
            }
        
    }
    
    
    // MARK:- This method setup the capture session for the device.
    fileprivate func SetupCaptureSession() {
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video as the media type parameter.
        let captureDevice = AVCaptureDevice.default(for: .video)
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            
            // Set the input device on the capture session.
            captureSession?.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            view.layer.addSublayer(videoPreviewLayer!)
            
            // Start video capture.
            captureSession?.startRunning()
            
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
                qrCodeFrameView.layer.borderWidth = 2
                view.addSubview(qrCodeFrameView)
                view.bringSubview(toFront: qrCodeFrameView)
            }
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
    }
  
    
    func activityIndicator(_ title: String) {
        
        strLabel.removeFromSuperview()
        activityIndicator.removeFromSuperview()
        effectView.removeFromSuperview()
        
        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 160, height: 46))
        strLabel.text = title
        strLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        strLabel.textColor = UIColor(white: 0.9, alpha: 0.7)
        
        effectView.frame = CGRect(x: view.frame.midX - strLabel.frame.width/2, y: view.frame.midY - strLabel.frame.height/2 , width: 160, height: 46)
        effectView.layer.cornerRadius = 15
        effectView.layer.masksToBounds = true
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        activityIndicator.startAnimating()
        
        effectView.contentView.addSubview(activityIndicator)
        effectView.contentView.addSubview(strLabel)
        view.addSubview(effectView)
    }

    
    func postDataToServer(qrcode : String) {
        self.activityIndicator("Saving Data")
        //Call API for data Save and remove effectView
        self.effectView.removeFromSuperview()
    }
}

extension QRCodeVC : AVCaptureMetadataOutputObjectsDelegate{
    

    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if  metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            qrResult = "No QR/barcode is detected"
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil && qrResult == nil {
                self.videoPreviewLayer?.removeFromSuperlayer()
                self.qrCodeFrameView?.isHidden = true
                displayView.isHidden = false
                resultLabel.isHidden = false
                result.isHidden = false
                qrResult = metadataObj.stringValue!
                resultLabel.text = qrResult
//                self.postDataToServer(qrcode: qrResult!)
            }
        }
    }
}
