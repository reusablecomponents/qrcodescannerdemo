//
//  EventManager.swift
//  LoginSignUpReusable
//
//  Created by Pooja Rana on 30/01/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit
import Reachability
import MBProgressHUD
import AVFoundation


typealias ActionHandler = ((String?) -> Void)?

class EventManager: NSObject {
    
    // MARK: -  Show Native Alert 
    class func showAlert(alertMessage: String?, btn1Tit: String?,  btn2Tit: String?, sender: UIViewController, action: ActionHandler, style: Int = 0) { // 0 for
        
        let alert = UIAlertController(title: ConstantModel.ProjectName, message: alertMessage, preferredStyle: (style == 1) ? .actionSheet : .alert)
        if btn1Tit != nil {
            alert.addAction(UIAlertAction(title: btn1Tit, style: UIAlertActionStyle.default, handler : {(alert: UIAlertAction!) in
                action?(btn1Tit)
            }))
        }
        if btn2Tit != nil {
            alert.addAction(UIAlertAction(title: btn2Tit, style: UIAlertActionStyle.default, handler : {(alert: UIAlertAction!) in
                action?(btn2Tit)
            }))
        }
        
        if style == 1 {
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler : nil))
        }
        sender.present(alert, animated: true, completion: nil)
    }
    class func showAlertWithMultipleButtons(alertMessage: String?, buttonTitles: [String], sender: UIViewController, action: ActionHandler, style: Int = 0) { // 0 for
        
        let alert = UIAlertController(title: ConstantModel.ProjectName, message: alertMessage, preferredStyle: (style == 1) ? .actionSheet : .alert)
        
        for buttonTitle in buttonTitles {
            alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.default, handler : {(alert: UIAlertAction!) in
                action?(buttonTitle)
            }))
        }
        
        if style == 1 {
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler : nil))
        }
        sender.present(alert, animated: true, completion: nil)
    }
    
    // MARK: -  Check for Internet 
    class func checkForInternetConnection() -> Bool {
        return ((Reachability()?.connection)! == .none) ? false : true
    }
    
    // MARK: -  Loader 
    class func showloader() {
        MBProgressHUD.showAdded(to: (ConstantModel.appDelegate.window)!, animated: true)
    }
    
    class func hideloader() {
        MBProgressHUD.hide(for: (ConstantModel.appDelegate.window)!, animated: true)
    }
    
    // MARK: -  DatePicker 
    class func inputV(_ inputVV: UIView, sender: Any?) -> UIView {
        let inputView = UIView(frame:inputVV.frame)
        inputVV.frame.size.width = UIScreen.main.bounds.width
        inputView.addSubview(inputVV)
        inputView.addSubview(self.toolBar(sender))
        return inputView
    }
    
    class func toolBar (_ target : Any?) -> UIToolbar {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 255/255, green: 92/255, blue: 54/255, alpha: 1)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.done, target: target, action: NSSelectorFromString("resignTextField:"))
        cancelButton.tag = 100
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: target, action: NSSelectorFromString("resignTextField:"))
        doneButton.tag = 101
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        return toolBar
    }
    
    
    // MARK: -  Download image from URL 
    class func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    // MARK: -  Download thumbnail image from URL 
    class func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        
        return nil
    }
    
}
